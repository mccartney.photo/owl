# Use latest Fedora instance
FROM fedora:latest

# Install required system packages
dnf install python3
dnf install apache

# Copy application files into the container
ADD . /var/www/Owl

# Change the working directory to the application directory
WORKDIR /var/www/Owl

# Install python packages
RUN pip install -r requirements.txt

# Expose port 80
EXPOSE 80

# Define environment variables
ENV DJANGO_SETTINGS_FILE Owl.settings



