"""
Module managing the update_prices custom command.
"""
from django.db.models import Q
from django.core.management.base import BaseCommand
from mtg.views import update_price_file
import mtg.models


class Command(BaseCommand):
    """
    Custom manage.py command for update_prices
    """
    help = 'A description of your command'

    def add_arguments(self, parser):
        """
        Method to add arguments to the update_prices command.
        :param parser:
        :return:
        """
        parser.add_argument(
            '--format', dest='formats', help='Formats to be updated.',
            action='append', default=[]
        )

    def handle(self, *args, **options):
        """
        Method to control the behaviour of the update_prices command.
        :param args:
        :param options:
        :return:
        """
        cards = mtg.models.Card.objects
        eternal_cards = [
            'Swamp', 'Island', 'Plains', 'Mountain', 'Forest', 'Evolving Wilds'
        ]
        cards = cards.exclude(name__in=eternal_cards)
        formats = options['formats']
        filters = Q()

        # Filter by formats if provided
        if 'standard' in formats:
            filters = filters | Q(standard_legal=True)
        if 'modern' in formats:
            filters = filters | Q(modern_legal=True)
        if 'legacy' in formats:
            filters = filters | Q(legacy_legal=True)
        if 'vintage' in formats:
            filters = filters | Q(vintage_legal=True)
        cards = cards.filter(filters)

        # Update the prices
        for card in cards.all():
            card.update_price()

        update_price_file()
