"""
Serializers for the django rest framework.
"""
from rest_framework import serializers

from mtg.models import Print, Card, Set


class CardSerializer(serializers.ModelSerializer):
    """
    Serializer for the card model.
    """
    class Meta:
        model = Card
        fields = (
            'name', 'rarity', 'standard_legal', 'modern_legal', 'legacy_legal',
            'vintage_legal'
        )


class SetSerializer(serializers.ModelSerializer):
    """
    Serializer for set model.
    """
    class Meta:
        model = Set
        fields = ('name', 'goldfish_name', 'code', 'release_date')


class PrintSerializer(serializers.ModelSerializer):
    """
    Serializer for print model.
    """
    # set = SetSerializer()
    # card = CardSerializer()

    class Meta:
        model = Print
        fields = ('goldfish_price', 'sell_price', 'buy_price', 'buy_quantity')
