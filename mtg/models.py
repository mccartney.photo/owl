"""
Models for the mtg pricing app.
"""

import time

import mtgsdk
import requests
from bs4 import BeautifulSoup
from django.db import models


class Set(models.Model):
    """
    Set model to track all Magic the Gathering sets.
    """
    name = models.CharField(max_length=64)
    code = models.CharField(max_length=8, unique=True)
    goldfish_name = models.CharField(max_length=64, null=True, blank=True)
    release_date = models.DateField(null=True)
    get_online_price = models.BooleanField(default=False)

    @classmethod
    def update_sets(cls):
        """
        A class method to update the sets using the mtgsdk library.
        :return:
        """
        sets = mtgsdk.Set.all()
        for set_obj in sets:
            Set.objects.update_or_create(
                code=set_obj.code, defaults={
                    'name': set_obj.name, 'release_date': set_obj.release_date
                }
            )


class Card(models.Model):
    """
    Model to track all of the distinct cards.
    """
    name = models.CharField(max_length=256, unique=True)
    rarity = models.CharField(max_length=32)
    standard_legal = models.BooleanField()
    modern_legal = models.BooleanField()
    legacy_legal = models.BooleanField()
    vintage_legal = models.BooleanField()
    sets = models.ManyToManyField(Set, through='Print')

    @classmethod
    def update_cards(cls):
        """
        A class method to update all cards listed in the mtgsdk library.
        :return:
        """
        cards = mtgsdk.Card
        for sdkcard in cards.all():
            try:
                print('Importing', sdkcard.name, sdkcard.set)

                card_legality = {
                    legality['format']: legality['legality']
                    for legality in sdkcard.legalities
                }

                defaults = {
                    'rarity': sdkcard.rarity,
                    'standard_legal': bool(card_legality.get('Standard')),
                    'modern_legal': bool(card_legality.get('Modern')),
                    'legacy_legal': bool(card_legality.get('Legacy')),
                    'vintage_legal': bool(card_legality.get('Vintage'))
                }

                card, _ = Card.objects.update_or_create(
                    name=sdkcard.name, defaults=defaults
                )
                mtgset, _ = Set.objects.update_or_create(
                    code=sdkcard.set
                )
                print_obj, created = Print.objects.update_or_create(
                    card=card, set=mtgset
                )

                if mtgset.get_online_price and not created:
                    print_obj.update_price()
                    time.sleep(3)
            except Exception as err:
                print(sdkcard.name)
                time.sleep(3)
                raise err

    def update_price(self):
        """
        An instance method to update the price for every printing for a given
        card.
        :return:
        """
        prints = self.print_set
        prints = prints.filter(set__get_online_price=True)
        for cardprint in prints.all():
            cardprint.update_price()
            time.sleep(5)


class Print(models.Model):
    """
    Print class to track each unique printing of a given card for each of it's
    sets.
    """
    set = models.ForeignKey(Set, on_delete=models.CASCADE)
    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    goldfish_price = models.FloatField(null=True, blank=True)
    buy_price = models.FloatField(null=True, blank=True)
    sell_price = models.FloatField(null=True, blank=True)
    buy_quantity = models.IntegerField(null=True, blank=True)

    class Meta:
        unique_together = (('set', 'card'),)

    @classmethod
    def update_standard_price(cls):
        """
        (DEPRECATED) Class method to update all standard cards.
        :return:
        """
        standard_prints = cls.objects.filter(
            card__standard_legal=True
        ).exclude(
            card__name__in=[
                'Mountain', 'Swamp', 'Island', 'Plains', 'Forest',
                'Evolving Wilds'
            ],
            card__rarity__in=['Common', 'Basic Land']
        )
        for cardprint in standard_prints.all():
            cardprint.update_price()
            time.sleep(1.5)

    @classmethod
    def update_all_price(cls):
        """
        (DEPRECATED) A class method to update all prices of all cards.
        :return:
        """
        all_prints = cls.objects.filter(
            goldfish_price__isnull=True
        ).exclude(
            card__name__in=[
                'Island', 'Mountain', 'Swamp', 'Forest', 'Plains',
                'Evolving Wilds'
            ]
        )
        for cardprint in all_prints.all():
            exceptions = UpdatePriceException.objects.filter(
                set=cardprint.set,
                card=cardprint.card
            ).exists()
            if exceptions.exists():
                continue
            cardprint.update_price()
            time.sleep(5)

    def update_price(self):
        """
        An instance method to update the price of a given card printing.
        :return:
        """
        card_name = self.card.name
        card_name = card_name.replace(' ', '+')
        card_name = card_name.replace('\'', '')
        card_name = card_name.replace(',', '')

        if self.set.goldfish_name:
            set_name = self.set.goldfish_name
        else:
            set_name = self.set.name
            set_name = set_name.replace(' ', '+')
            set_name = set_name.replace('\'', '')
            set_name = set_name.replace(',', '')

        if not card_name or not set_name:
            defaults = {'exception': 'Card name or set name is None.'}
            UpdatePriceException.objects.update_or_create(
                set=self.set, card=self.card, defaults=defaults
            )
            print('Exception:', defaults['exception'])
            return

        url = 'https://www.mtggoldfish.com/price/' \
              '{}/{}#online'.format(set_name, card_name)
        response = requests.get(url)

        if response.status_code == 404:
            defaults = {'exception': 'Url not working'}
            UpdatePriceException.objects.update_or_create(
                set=self.set, card=self.card, defaults=defaults
            )
            print('Exception:', defaults['exception'])
            print(url)
            return

        elif response.status_code == 429:
            print(response.status_code, response.body)
            raise Exception('Throttled!')

        elif response.status_code != 200:
            print(response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        if len((soup.findAll('div', {'class': 'price-box-price'}))) != 2:
            defaults = {'exception': 'Only one price listed on Goldfish'}
            UpdatePriceException.objects.update_or_create(
                set=self.set, card=self.card, defaults=defaults
            )
            print('Exceptions:', defaults['exception'])
            print(self.card.name, self.set.name)
            return

        val = soup.findAll('div', {'class': 'price-box-price'})[0].text
        self.goldfish_price = float(val)

        print(self.card.name, self.set.name, self.goldfish_price)

        self.save()
        UpdatePriceException.objects.filter(
            card=self.card, set=self.set
        ).delete()


class UpdatePriceException(models.Model):
    """
    Logging model to track card scraping errors.
    """
    set = models.ForeignKey(Set, on_delete=models.CASCADE)
    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    exception = models.CharField(max_length=256)
