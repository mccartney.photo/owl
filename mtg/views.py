"""
Views file for the mtg app.
"""
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from mtg.models import Card, Print, UpdatePriceException, Set
from mtg.serializers import PrintSerializer, SetSerializer


def home(request):
    """
    View to manage the home page
    :param request:
    :return:
    """
    set_prices = Print.objects.filter(
        Q(buy_price__isnull=False) | Q(sell_price__isnull=False)
    ).order_by('-set__release_date')
    context = {'set_prices': set_prices}
    return render(request, 'home.html', context)


def card_search(request):
    """
    View to manage searching for individual prints
    :param request:
    :return:
    """
    search_input = request.GET.get('card')
    set_name = request.GET.get('set')
    try:
        card = Card.objects.get(name__icontains=search_input)
    except (Card.DoesNotExist, Card.MultipleObjectsReturned):
        try:
            card = Card.objects.get(name=search_input)
        except (Card.DoesNotExist, Card.MultipleObjectsReturned):
            cards = Card.objects.filter(name__icontains=search_input)
            context = {'cards': cards, 'search_input': search_input}
            return render(request, r'mtg\multiple_cards_search.html', context)
    try:
        if set_name:
            print_obj = Print.objects.get(card=card, set__name=set_name)
        else:
            print_obj = Print.objects.get(card=card)
    except Print.MultipleObjectsReturned:
        prints = Print.objects.filter(card=card)
        context = {'prints': prints, 'card': card}
        return render(request, r'mtg\card_search.html', context)
    response = redirect(
        'mtg:cardprint', card=card.name, cardset=print_obj.set.name
    )
    return response


class SetView(APIView):
    """
    Restful API for the Set model.
    """
    renderer_classes = [TemplateHTMLRenderer]
    template_name = r'mtg\setform.html'

    def get(self, request, name):
        """
        GET method for the Set restful API
        :param request:
        :param name:
        :return:
        """
        cardset = get_object_or_404(Set, name=name)
        serializer = SetSerializer(cardset)
        return Response({'serializer': serializer, 'cardset': cardset})

    def post(self, request, name):
        """
        POST method for the Set restful API
        :param request:
        :param name:
        :return:
        """
        cardset = get_object_or_404(Set, name=name)
        serializer = SetSerializer(cardset, request.data)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'set': cardset})
        serializer.save()
        return Response({'serializer': serializer, 'cardset': cardset})


class CardPrintView(APIView):
    """
    Print restful API class.
    """
    renderer_classes = [TemplateHTMLRenderer]
    template_name = r'mtg\cardprintform.html'

    def get(self, request, card, cardset):
        """
        GET method for the Print model restful API
        :param request:
        :param card:
        :param cardset:
        :return:
        """
        cardprint = get_object_or_404(
            Print, card__name=card, set__name=cardset
        )
        cardprint.update_price()
        serializer = PrintSerializer(cardprint)
        return Response({'serializer': serializer, 'cardprint': cardprint})

    def post(self, request, card, cardset):
        """
        POST method for the Print model restful API
        :param request:
        :param card:
        :param cardset:
        :return:
        """
        cardprint = get_object_or_404(
            Print, card__name=card, set__name=cardset
        )
        serializer = PrintSerializer(cardprint, request.data)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'cardprint': cardprint})
        serializer.save()
        return Response({'serializer': serializer, 'cardprint': cardprint})


@csrf_exempt
def update_bot_prices(unused_request):
    """
    view to trigger the update of bot prices.
    :param unused_request:
    :return:
    """
    # update_price_file()
    return JsonResponse({})


def update_price_file():
    """
    function to update the price file based off of current data in the database
    :return:
    """
    with open(r'F:\mtgobot_folder\PersonalPrices.txt', 'w') as file:
        prints = Print.objects.exclude(
            card__name__in=[
                'Mountain', 'Swamp', 'Island', 'Plains', 'Forest',
                'Evolving Wilds'
            ]
        ).all()
        for cardprint in prints:
            abbr = cardprint.set.code
            name = cardprint.card.name
            if cardprint.goldfish_price:
                if cardprint.sell_price:
                    sell_price = cardprint.sell_price
                else:
                    sell_price = cardprint.goldfish_price
                if cardprint.buy_price:
                    buy_price = cardprint.buy_price
                else:
                    buy_price = get_buy_price(cardprint.goldfish_price)
            else:
                sell_price = 1000
                buy_price = 0.001
            if cardprint.card.standard_legal or cardprint.buy_quantity:
                if cardprint.buy_quantity:
                    buy_quantity = str(cardprint.buy_quantity)
                else:
                    buy_quantity = str(4)
            else:
                buy_quantity = str(0)
            line = ''
            # Set
            line += abbr + ';'
            # Card
            line += name + ';'
            # Sell Price
            line += '{:.3f}'.format(sell_price) + ';'
            # Foil Sell Price
            line += '{:.3f}'.format(sell_price) + ';'
            # Buy Price
            line += '{:.3f}'.format(buy_price) + ';'
            # Foil Buy Price
            line += '{:.3f}'.format(buy_price) + ';'
            # Buy Quantity
            line += buy_quantity + ';'
            # Foil Buy Quantity
            line += str(0)
            line += '\n'
            file.write(line)


def get_buy_price(price):
    """
    function to set buy price based off of market price.
    :param price:
    :return:
    """
    return .7 * price
    # return (-0.6 * (0.32 ** price) + 0.7) * price


def price_exceptions(request):
    """
    view for checking all pricing exceptions.
    :param request:
    :return:
    """
    exceptions = UpdatePriceException.objects.all()
    context = {'exceptions': exceptions}
    return render(request, 'mtg/price_exceptions.html', context)
