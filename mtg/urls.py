from django.urls import path, re_path

import mtg.views

urlpatterns = [
    path('card_search/', mtg.views.card_search, name='card_search'),
    path('update_bot_prices/', mtg.views.update_bot_prices,
         name='update_bot_prices'),
    path('update_price_exceptions/', mtg.views.price_exceptions,
         name='update_price_exceptions'),
    re_path(
        '^cardprint/(?P<card>[A-z0-9 "\':.,\-—()]+)/(?P<cardset>[A-z0-9 "\':.,\-—()]+)/$',
        mtg.views.CardPrintView.as_view(),
        name='cardprint'),
    re_path('^cardset/(?P<name>[A-z0-9 "\':.,\-—()]+)/$',
            mtg.views.SetView.as_view(), name='cardset')
]
